# Generator MongoDB Atlas - Stitch

[![npm version](https://badge.fury.io/js/generator-mongodb-stitch.svg)](//npmjs.com/package/generator-mongodb-stitch)

This project was created for scaffold a Stitch app. Once you have created the app, you can deploy it with Gulp on three diferents environments (dev, stage, prod) to MongoDB Atlas as three separated Stitch apps.

## Prerequisites
Before you deploy the stitch app with Gulp, you must have installed the `stitch-cli`. Here is how you install it [Stitch CLI Documentation](https://docs.mongodb.com/stitch/import-export/stitch-cli-reference/)

Next step is to login

```bash
$ stitch-cli login --username=<your-atlas-username> --api-key=<your-api-key>
```

Gulp script will use that session in future deploys.

## Installing

In case you haven't installed the generator yet:

```bash
$ npm install -g yeoman generator-mongodb-stitch
```

Using the generator:

```bash
$ yo mongodb-stitch
```

Provide app name and MongoDB Atlas project id as the yeoman ask you so.

Once the generator has finished the process, you will find the basic structure of your app and the gulpfile for dpeloyment.

## Creating the API

Every Stitch app may have several services. We use the HTTP service for build an API for the Stitch app. The HTTP service allow us to add several `incoming_webhooks`. Every webhook it's an API endpoint.

To create te API we run

```bash
$ yo mongodb-stitch:api
```

Yeoman will ask us for an api name. In addition we must provide the name of the `MongoDB Atlas Cluster` where we will find or create our database.

The database may or may not exists in our cluster, that wont affect the creation of the API.

Lastly we must indicate a default collection for our API.

## Adding a Entity

By adding an Entity to our API we will create the collection for that Entity in our database. Then, yeoman will create three endpoints Create, List and Details.

```bash
$ yo mongodb-stitch:entity
```

Entity name can only contain alphabets. You may add all the entities that you need per API.

## Deployment

You can deploy to tree diferents environments, in the practice that's three diferentes stitch apps on the Atlas project.

Feel free to deploy to any env at any time `gulp deploy:dev`, `gulp deploy:stage` or `gulp deploy:prod`.

For instance
```bash
$ gulp deploy:prod
```

Will dpeloy in your Atlas proyect the stitc app named ***app-name*-prod**.

## Authors
- David Méndez - Initial work - [@mitlley](https://gitlab.com/mitlley).
