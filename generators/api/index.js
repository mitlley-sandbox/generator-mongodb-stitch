'use strict';
var path = require('path');
var Generator = require('yeoman-generator');
var yosay = require('yosay');
var chalk = require('chalk');
var _ = require('lodash');

module.exports = class extends Generator {
  // The name `constructor` is important here
  constructor(args, opts) {
    // Calling the super constructor is important so our generator is correctly set up
    super(args, opts);

    // Next, add your custom code
    this.option('babel'); // This method adds support for a `--babel` flag
  }

  initializing() {
    // Have Yeoman greet the user.
    var appName = this.config.get('appName');
    if(appName == undefined || appName == ""){
      this.log(yosay(
        'It seems you haven\'t created the Stitch app yet! Please, run ' + chalk.yellow('yo mongodb-stitch') + ' first.'
      ));
      process.exit();
    } else {
      this.log(yosay(
        'Let\'s create an API for the ' + chalk.red(appName) + ' app!'
      ));
    }

  }

  async prompting() {
    var that = this;

    return this.prompt([{
      type: 'input',
      name: 'apiName',
      message: 'What\'s the API name?',
      default: this.config.get('apiName') || this.config.get('appName') + '-api',
      validate: function(input){
        if(input.indexOf(' ') !== -1){
          return "API name can\'t contain spaces.";
        } else if(input.trim() == ""){
          return "API name it\'s required.";
        }

        return true;
      }
    },
    {
      type: 'input',
      name: 'clusterName',
      message: 'What\'s the MongoDB Atlas Cluster name?',
      default: this.config.get('clusterName'),
      validate: function(input){
        if(input.indexOf(' ') !== -1){
          return "MongoDB Atlas Cluster name can\'t contain spaces.";
        } else if(input.trim() == ""){
          return "MongoDB Atlas Cluster name it\'s required.";
        }

        return true;
      }
    },
    {
      type: 'input',
      name: 'databaseName',
      message: 'What\'s the database name?',
      default: this.config.get('databaseName')  || this.config.get('appName') + '-database',
      validate: function(input){
        if(input.indexOf(' ') !== -1){
          return "The database name can\'t contain spaces.";
        } else if(input.trim() == ""){
          return "The database name it\'s required.";
        }

        return true;
      }
    },
    {
      type: 'input',
      name: 'defaultCollectionName',
      message: 'What\'s the default collection name?',
      default: this.config.get('defaultCollectionName'),
      validate: function(input){
        if(input.indexOf(' ') !== -1){
          return "The default collection name can\'t contain spaces.";
        } else if(input.trim() == ""){
          return "The default collection name it\'s required.";
        }

        return true;
      }
    }]).then(function (props) {
      that.log("Nice! Let\'s make " + chalk.blue(props.apiName));
      that.config.set({
        apiName: props.apiName,
        clusterName: props.clusterName,
        databaseName: props.databaseName,
        defaultCollectionName: props.defaultCollectionName
      });
      that.config.save();
      that.props = props;
    });
  }

  writing() {
    var props = this.props;

    var copy = this.fs.copy.bind(this.fs);
    var copyTpl = this.fs.copyTpl.bind(this.fs);
    var tPath = this.templatePath.bind(this);
    var dPath = this.destinationPath.bind(this);

    var apipath = function (filename) {
      return path.join('services', props.apiName, filename);
    };

    copyTpl(tPath('_config.json'), dPath(apipath('config.json')), props);
    copyTpl(tPath(path.join('services', 'mongodb-atlas', '_config.json')), dPath(path.join('services', 'mongodb-atlas', 'config.json')), props);
    copyTpl(tPath(path.join('services', 'mongodb-atlas', 'rules', '_database.collection.json')), dPath(path.join('services', 'mongodb-atlas', 'rules', props.databaseName + '.' + props.defaultCollectionName + '.json')), props);
  }
}
