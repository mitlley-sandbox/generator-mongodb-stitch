'use strict';
var path = require('path');
var Generator = require('yeoman-generator');
var yosay = require('yosay');
var chalk = require('chalk');
var _ = require('lodash');

module.exports = class extends Generator {
  // The name `constructor` is important here
  constructor(args, opts) {
    // Calling the super constructor is important so our generator is correctly set up
    super(args, opts);

    // Next, add your custom code
    this.option('babel'); // This method adds support for a `--babel` flag
  }

  initializing() {
    // Have Yeoman greet the user.
    var appName = this.config.get('appName');
    var apiName = this.config.get('apiName');
    if(appName == undefined || appName == ""){
      this.log(yosay(
        'It seems you haven\'t created the Stitch app yet! Please, run ' + chalk.yellow('yo mongodb-stitch') + ' first.'
      ));
      process.exit();
    } else if(apiName == undefined || apiName == ""){
      this.log(yosay(
        'It seems you haven\'t created an API for your app yet! Please, run ' + chalk.yellow('yo mongodb-stitch:api') + ' first.'
      ));
      process.exit();
    } else {
      this.log(yosay(
        'Let\'s add an entity to your ' + chalk.red(apiName) + ' API!'
      ));
    }

  }

  async prompting() {
    var that = this;

    return this.prompt([{
      type: 'input',
      name: 'entityName',
      message: 'What\'s the Entity name?',
      default: this.config.get('entityName'),
      validate: function(input){
        var regex = /^[a-zA-Z]*$/;
        if (!regex.test(input)) {
          return "The Entity name acepts alphabets only.";
        } else if(input.indexOf(' ') !== -1){
          return "The Entity name can\'t contain spaces.";
        } else if(input.trim() == ""){
          return "The Entity name it\'s required.";
        }

        return true;
      }
    }]).then(function (props) {
      that.log("Nice! Let\'s add it.");
      that.config.set({
        entityName: props.entityName
      });
      that.config.save();
      that.props = props;
    });
  }

  writing() {
    // Really importants configs
    var databaseName = this.config.get('databaseName');
    var apiName = this.config.get('apiName');

    var props = this.props;
    props.databaseName = databaseName;

    var copy = this.fs.copy.bind(this.fs);
    var copyTpl = this.fs.copyTpl.bind(this.fs);
    var tPath = this.templatePath.bind(this);
    var dPath = this.destinationPath.bind(this);

    var webhookpath = function (api, entity, action, filename) {
      return path.join('services', api, 'incoming_webhooks', entity + action, filename);
    };

    copyTpl(tPath(webhookpath('http', 'entity', 'Create', '_config.json')), dPath(webhookpath(apiName, props.entityName, 'Create', 'config.json')), props);
    copyTpl(tPath(webhookpath('http', 'entity', 'Create', '_source.js')), dPath(webhookpath(apiName, props.entityName, 'Create', 'source.js')), props);

    copyTpl(tPath(webhookpath('http', 'entity', 'List', '_config.json')), dPath(webhookpath(apiName, props.entityName, 'List', 'config.json')), props);
    copyTpl(tPath(webhookpath('http', 'entity', 'List', '_source.js')), dPath(webhookpath(apiName, props.entityName, 'List', 'source.js')), props);

    copyTpl(tPath(webhookpath('http', 'entity', 'Details', '_config.json')), dPath(webhookpath(apiName, props.entityName, 'Details', 'config.json')), props);
    copyTpl(tPath(webhookpath('http', 'entity', 'Details', '_source.js')), dPath(webhookpath(apiName, props.entityName, 'Details', 'source.js')), props);
  }
}
