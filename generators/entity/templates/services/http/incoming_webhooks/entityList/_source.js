/*
 * Function generated with Yeoman Generator MongoDB Atlas - Stitch app.
 */

// Try running in the console below.

exports = function(payload) {
  const mongodb = context.services.get("mongodb-atlas");
  const <%= entityName %>Collection = mongodb.db("<%= databaseName %>").collection("<%= entityName %>");

  var <%= entityName %> = <%= entityName %>Collection.find({}).toArray();
  return <%= entityName %>;
};
