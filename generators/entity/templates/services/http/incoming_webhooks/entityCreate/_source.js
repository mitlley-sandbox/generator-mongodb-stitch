/*
 * Function generated with Yeoman Generator MongoDB Atlas - Stitch app.
 */

// Try running in the console below.

exports = function(payload) {
  const mongodb = context.services.get("mongodb-atlas");
  const <%= entityName %>Collection = mongodb.db("<%= databaseName %>").collection("<%= entityName %>");

  var <%= entityName %> = {};
  var response = {
    status: '0',
    message: "Error creating <%= entityName %>."
  };

  if(payload.body){
    <%= entityName %> = EJSON.parse(payload.body.text());
    <%= entityName %>Collection.insertOne(<%= entityName %>);

    response.status = '1';
    response.message = "<%= entityName %> created.";
  }

  return response;
};
