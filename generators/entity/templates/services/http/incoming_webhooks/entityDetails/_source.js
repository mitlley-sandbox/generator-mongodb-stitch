/*
 * Function generated with Yeoman Generator MongoDB Atlas - Stitch app.
 */

// Try running in the console below.

exports = function(payload) {
  const mongodb = context.services.get("mongodb-atlas");
  const <%= entityName %>Collection = mongodb.db("<%= databaseName %>").collection("<%= entityName %>");

  if(payload.query.id){
    var <%= entityName %>Details = <%= entityName %>Collection.findOne({'_id': BSON.ObjectId(payload.query.id)});
    return <%= entityName %>Details;
  }

  return {};
};
